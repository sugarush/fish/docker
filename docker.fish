if ! test -f $sugar_modules_config_directory/docker.fish
  cp $argv[1]/config/docker.fish $sugar_modules_config_directory/docker.fish
end

source $sugar_modules_config_directory/docker.fish

function docker-machine-init -d 'Initialize a docker-machine.'
  set -l machine $argv[1]
  test -z $machine; and set machine -l $docker_default_machine
  docker-machine create --driver virtualbox $machine
end

function docker-machine-start -d 'Start a docker-machine.'
  set -l machine $argv[1]
  test -z $machine; and set -l machine $docker_default_machine
  docker-machine start $machine
end

function docker-machine-stop -d 'Stop a docker-machine.'
  set -l machine $argv[1]
  test -z $machine; and set -l machine $docker_default_machine
  docker-machine stop $machine
end

function docker-machine-env -d 'Set the docker-machine environment.'
  set -l machine $argv[1]
  test -z $machine; and set -l machine $docker_default_machine
  eval (docker-machine env $machine)
end

function docker-machine-ip -d 'Get the IP address of a docker-machine.'
  set -l machine $argv[1]
  test -z $machine; and set -l machine $docker_default_machine
  docker-machine env $machine \
    | grep 'DOCKER_HOST' \
    | sed -E 's@^set -gx DOCKER_HOST "tcp://(.*):.*";$@\1@'
end

function docker-build -d 'Build a docker image.'
  set -l tag $argv[1]
  test -z $tag; and set -l tag default
  set -l dockerfile $argv[2]
  test -z $dockerfile; and set -l dockerfile Dockerfile
  docker build -t $tag -f $dockerfile (pwd)
end

function docker-clean-containers -d 'Remove exited docker containers.'
  set -l containers (docker ps --all --quiet --format '{{.Names}}' \
    --filter 'status=exited')
  for container in $containers
    docker rm $container
  end
end

function docker-clean-images -d 'Remove dangling images.'
  set -l images (docker images --all --quiet --filter 'dangling=true')
  for image in $images
    docker rmi $image
  end
end
